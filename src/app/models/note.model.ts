export class Note {
    id: number;
    title : string;
    description: string;
    createdBy : string;
    createdOn : string;
    category : string;
    priority : boolean;
}